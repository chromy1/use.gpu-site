import React, { useCallback } from 'react';
import { useRefineCursor, Cursor } from '../util/cursor';

export type ExpandState = Record<number, boolean>;

export type ExpandableProps = {
  id: string | number,
  initialValue: boolean,
  expandCursor: Cursor<ExpandState>,
  children: (expand: boolean, onClick: (e: any) => void) => React.ReactElement,
}

export const Expandable: React.FC<ExpandableProps> = ({id, initialValue = false, expandCursor, children}) => {
  let [expand, updateExpand] = useRefineCursor<boolean>(expandCursor)(id);

  if (expand === undefined) expand = initialValue;

  const onClick = useCallback((e: any) => {
    updateExpand(expand === false);
    e.preventDefault();
    e.stopPropagation();
  }, [expand, updateExpand]);

  return children(expand, onClick);
}
