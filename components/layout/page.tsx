import React, { FC, useEffect, useRef, useState } from 'react';
import { styled } from '../../lib/stitches.config';
import { STYLES, FONTS, COLORS, GRID } from './constants';

const ICON = (s: string) => <span className="m-icon">{s}</span>;

export const FullRow = styled('div', {
  display: 'flex',
  width: '100%',
  height: '100%',
  flexGrow: 1,
  flexShrink: 1,
});

export const FullColumn = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  width: '100%',
  height: '100%',
  flexGrow: 1,
  flexShrink: 1,
  position: 'relative',
});

export const TopBar = styled('div', {
  ...STYLES.shadowLow,
  color: COLORS.textHeader,
  background: COLORS.bgMasthead,
  width: '100%',
  height: '50px',
  display: 'flex',
  position: 'relative',
  zIndex: 100,
  flexShrink: 0,
});

export const LeftBar = styled('div', {
  ...FONTS.bodyText,
  color: COLORS.textBody,
  background: COLORS.bgPanel,
  borderRight: COLORS.borderDivider,
  width: '320px',
  flexShrink: 0,
  position: 'relative',
  zIndex: 2,

  'body.animated &': {
    transition: 'transform 0.25s ease-in-out',
  },
    
  "@media (max-width: 768px)": {
    ...STYLES.shadowLow,
    transform: 'translate(-100%, 0)',
    zIndex: 100,
  },
  "variants": {
    open: {
      true: {
        transform: 'none',
      },
    },
  }
});

export const Main = styled('div', {
  ...FONTS.bodyText,
  color: COLORS.textBody,
  background: COLORS.bgBody,
  borderRight: COLORS.borderDivider,
  width: '100%',
  minHeight: '100%',
  marginLeft: '0px',
  margiRight: '0px',
  'body.animated &': {
    transition: 'margin-left 0.25s ease-in-out, margin-right 0.25s ease-in-out',
  },
  "@media (max-width: 768px)": {
    marginLeft: '-320px',
  },
  "@media (max-width: 1259px)": {
    marginRight: '-240px',
  },
});

export const RightBar = styled('div', {
  ...FONTS.bodyText,
  color: COLORS.textBody,
  width: '240px',
  flexShrink: 0,
  position: 'relative',
  zIndex: 2,

  "@media (min-width: 1440px)": {
    width: '300px',
  },

  'body.animated &': {
    transition: 'width 0.25s ease-in-out, transform 0.25s ease-in-out',
  },
  "@media (max-width: 1259px)": {
    background: COLORS.bgPanel,
    ...STYLES.shadowLow,
    transform: 'translate(100%, 0)',
  },
  "variants": {
    open: {
      true: {
        transform: 'none',
      },
    },
  },
});

export const StickyButtons = styled('div', {
  position: 'absolute',
  pointerEvents: 'none',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  "@media (min-width: 1260px)": {
    display: 'none',
  },
  '& > div': {
    height: '100%',
  },
  '& > div > div': {
    display: 'flex',
    justifyContent: 'space-between',
    position: 'sticky',
    top: 0,
    lineHeight: '40px',
  },
  zIndex: 101,
});

export const StickyButtonsShadow = styled('div', {
  position: 'absolute',
  pointerEvents: 'none',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  "@media (min-width: 1260px)": {
    display: 'none',
  },
  '& > div': {
    height: '100%',
  },
  '& > div > div': {
    display: 'flex',
    justifyContent: 'space-between',
    position: 'sticky',
    top: 0,
    lineHeight: '40px',
  },
  zIndex: 1,
  '& > div > div > span': {
    background: 'rgba(32, 32, 32, .75)',
    width: 48,
    height: 48,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  }
});

export const Hamburger = styled('div', {
  pointerEvents: 'auto',
  color: COLORS.textBody,
  display: 'none',
  cursor: 'default',
  borderRadius: 3,
  "@media (max-width: 768px)": {
    display: 'block',
    textAlign: 'center',
    paddingLeft: GRID.m - 6,
    paddingRight: GRID.m - 6,
  },
  margin: 5,
  userSelect: 'none',
  '&:hover': {
    background: COLORS.bgShine,
  },
  '& > span': {
    position: 'relative',
    top: 3,
  },
});

export const Outliner = styled('div', {
  pointerEvents: 'auto',
  color: COLORS.textBody,
  display: 'none',
  cursor: 'default',
  borderRadius: 3,
  "@media (max-width: 1259px)": {
    display: 'block',
    textAlign: 'center',
    paddingLeft: GRID.m - 6,
    paddingRight: GRID.m - 6,
  },
  margin: 5,
  userSelect: 'none',
  '&:hover': {
    background: COLORS.bgShine,
  },
  '& > span': {
    position: 'relative',
    top: 3,
  },
});

export const Container = styled('div', {
  flexGrow: 1,
  width: '100%',
  maxWidth: 1500,
  margin: '0 auto',
  position: 'relative',
});

export const Flex = styled('div', {
  display: 'flex',
  alignItems: 'center',
  height: '50px',
});

export const Grow = styled('div', {
  flexGrow: 1,
});
type PageFrameProps = {
  top: React.ReactNode,
  left: React.ReactNode,
  right: React.ReactNode,
  main: React.ReactNode,
};

export const PageFrame: FC<PageFrameProps> = (props: PageFrameProps) => {
  const {top, left, main, right} = props;

  const divRef = useRef<HTMLDivElement>();

  const [leftOpen, setLeftOpen] = useState<boolean>(false);
  const [rightOpen, setRightOpen] = useState<boolean>(false);
  const toggleLeftOpen = () => { setLeftOpen(o => !o); setRightOpen(false); }
  const toggleRightOpen = () => { setRightOpen(o => !o); setLeftOpen(false); }

  const close = () => {
    setLeftOpen(false);
    setRightOpen(false);
  };
  const maybeClose = (e: any) => {
    let el = e.target;
    do {
      if (el.tagName == 'A') {
        return close();
      }
    } while (el = el.parentElement);
  };

  const hamburger = (
    <Hamburger onClick={toggleLeftOpen} title="Index">{ICON('menu')}</Hamburger>
  );

  const outliner = (
    <Outliner onClick={toggleRightOpen} title="Sections">{ICON('format_list_numbered')}</Outliner>
  );

  useEffect(() => {
    document.body.classList.add('animated');
  }, []);

  return (<>
    <FullColumn>
      <TopBar>
        <Container>
          <Flex>
            {top}
          </Flex>
        </Container>
      </TopBar>
      <Container>
        <FullRow>
          <LeftBar open={leftOpen} onClick={maybeClose}>{left}</LeftBar>
          <Main onClick={close}>{main}</Main>
          <RightBar open={rightOpen} onClick={maybeClose}>{right}</RightBar>
        </FullRow>
      </Container>
      <StickyButtonsShadow><div><div>
        <span /><span />
      </div></div></StickyButtonsShadow>
      <StickyButtons><div><div>
        {hamburger}
        <Grow />
        {outliner}
      </div></div></StickyButtons>  
    </FullColumn>
  </>);
};