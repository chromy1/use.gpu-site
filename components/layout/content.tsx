import React, { useEffect, useRef, FC } from 'react';
import { styled } from '../../lib/stitches.config';
import router from 'next/router';

import { STYLES, FONTS, COLORS, GRID } from './constants';

export const Panel = styled('div', {
  paddingLeft: GRID.l * 1.5,
  paddingRight: GRID.l * 1.5,
  paddingBottom: GRID.l,
  paddingTop: 0,
  
  "@media (max-width: 768px)": {
    paddingLeft: GRID.m * 1.5,
    paddingRight: GRID.m * 1.5,
  },
  
  margin: '0 auto',
  maxWidth: 960,
});

type Props = {
  html: string,
};

export const Content: FC<Props> = ({html}: Props) => {
  const divRef = useRef<HTMLDivElement>(null);
  
  useEffect(() => {
    const {current: div} = divRef;
    if (!div) return;

    const handleClick = (e: any) => {
      if (e.target && e.button == 0 && !e.ctrlKey && !e.shiftKey && !e.metaKey) {
        const href = e.target.getAttribute('href');
        if (href && href[0] === '/') {
          router.push(href);
          e.preventDefault();
          e.stopPropagation();
        }
      }
    };

    const els = Array.from(div.querySelectorAll('a[href]'));
    for (const el of els) {
      el.addEventListener('click', handleClick);
    }
    return () => {
      for (const el of els) {
        el.removeEventListener('click', handleClick);
      }
    };
  }, [html]);
  
  return (
    <Panel>
      <div ref={divRef} className="typography markdown-content" dangerouslySetInnerHTML={{ __html: html }} />
    </Panel>
  );
};