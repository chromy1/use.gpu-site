import React, { FC, useRef } from 'react';
import { styled } from '../lib/stitches.config';
import { GRID, COLORS } from './layout/constants';

import { DocumentTree, DocumentMatch} from '../lib/types';
import { findIndexedDocuments, getIndexedDocumentTree } from '../lib/indexed';

import { Tree } from './layout/tree';
import { Padded, Inset } from './layout/ui';
import { Results } from './layout/results';
import { useUpdateState } from './util/cursor';
import { useRefineCursor } from './util/cursor';
import { useIsomorphicLayoutEffect } from './util/iso';

import { ExpandState } from './layout/expandable';

import { Search } from './search';
import type { NextPage } from 'next';
import { useRouter } from 'next/router';

type Props = {
  selected: string[],
};

export const Container = styled('div', {
  height: '100%',
});

export const LeftShim = styled('div', {
  position: 'absolute',
  top: 0,
  left: -2000,
  right: 0,
  bottom: 0,
  background: COLORS.bgPanel,
});

export const Scroller = styled(Padded, {
  position: 'relative',
  height: '100%',
  overflowY: 'scroll',

  backgroundColor: COLORS.bgPanel,
  
  '&::-webkit-scrollbar-thumb': {
    width: 8,
    height: 8,
    borderRadius: 4,
  },
});

export type SearchState = {
  query: string,
  selectedIndex: number,
  focused: boolean,
};

export const Outline: FC<Props> = (props: Props) => {
  const {selected} = props;
  const router = useRouter();
  const docTree = getIndexedDocumentTree();

  const expandCursor = useUpdateState<ExpandState>({});
  const searchCursor = useUpdateState<SearchState>({
    query: '',
    selectedIndex: 0,
    focused: false,
  });

  const divRef = useRef<HTMLDivElement>(null);
  
  const useSearchCursor = useRefineCursor(searchCursor);
  const [query] = useSearchCursor<string>('query');
  const [index, updateIndex] = useSearchCursor<number>('selectedIndex');
  const [focused] = useSearchCursor<boolean>('focused');
  const onSubmit = (match: DocumentMatch) => (match.doc.href) && router.push(match.doc.href);

  let view: React.ReactNode = null;
  let matches: DocumentMatch[] = [];
  if (query != '') {
    matches = findIndexedDocuments(query);

    const selectedIndex = Math.max(0, Math.min(matches.length - 1, index));
    if (index !== selectedIndex) updateIndex(selectedIndex);

    const selectedId = selected[selected.length - 1];
    const activeId = focused ? matches[selectedIndex]?.doc.slug : '---';
    
    view = (
      <Results matches={matches} selected={selectedId} active={activeId} />
    );
  }
  else {
    const selectedIndex = 0;
    if (index !== selectedIndex) updateIndex(selectedIndex);

    view = (
      <Tree expandCursor={expandCursor} outline={docTree} selected={selected} />
    );
  }

  useIsomorphicLayoutEffect(() => {
    const {current: div} = divRef;
    if (!div) return;

    const rowSelected = div.querySelector('.row-selected');
    if (!rowSelected) return;
    
    const rect = rowSelected.getBoundingClientRect();
    const min = 164;
    const max = window.innerHeight - 52;

    let scrollY = 0;
    if (rect.top < min) scrollY = rect.top - min;
    if (rect.bottom > max) scrollY = rect.bottom - max;
    
    div.children[0].scrollTop += scrollY;
  }, [query]);

  return (<Container>
    <LeftShim />
    <div ref={divRef} style={{top: 0, height: '100vh', position: 'sticky'}}>
      <Scroller style={{paddingTop: 0, paddingBottom: GRID.l * 2.5}}>
        <LeftShim />
        <Inset style={{position: 'relative', zIndex: 2}}>
          <Search matches={matches} searchCursor={searchCursor} onSubmit={onSubmit} />
          {view}
        </Inset>
      </Scroller>
    </div>
  </Container>)
}
