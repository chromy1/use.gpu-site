import React from 'react';

import { Meta } from '../../components/layout/meta';
import { PageFrame } from '../../components/layout/page';
import { Masthead } from '../../components/layout/masthead';
import { Content } from '../../components/layout/content';
import { Chapters } from '../../components/layout/chapters';

import { Outline } from '../../components/outline';
import type { NextPage } from 'next';

import { markdownToHTML } from '../../lib/markdown';
import { DocumentTree, Document } from '../../lib/types';
import { getDocumentBySlug } from '../../lib/document';
import { getAllIndexedDocuments, getPathBySlug } from '../../lib/indexed';
import { getPackageVersion } from '../../lib/gen/fs';

type Props = {
  doc: Document,
  path: string[],
  html: string,
  chapters: [string, number][],
  version: string,
};

const Doc: NextPage<Props> = (props: Props) => {
  const {doc, path, html, chapters, version} = props;

  return (<>
    <Meta title={doc.title} />
    <PageFrame
      top={<Masthead version={version} />}
      left={<Outline selected={path} />}
      main={<Content html={html} />}
      right={<Chapters chapters={chapters} />}
    />
  </>)
}

export const getStaticProps = async ({ params }: any) => {
  const version = getPackageVersion();

  const doc = getDocumentBySlug(params.slug);
  const path = getPathBySlug(params.slug);

  if (!doc) throw new Error(`page "${params.slug}" not found`);

  const [html, chapters] = await markdownToHTML(doc.content);

  return {
    props: { doc, path, html, chapters, version, },
  }
}

export async function getStaticPaths() {
  const docs = getAllIndexedDocuments().filter(d => d.slug !== '');
  
  return {
    paths: docs.map((doc: Document) => {
      return {
        params: {
          slug: doc.slug,
        },
      }
    }),
    fallback: false,
  }
}

export default Doc;
