import React from 'react';

import { Meta } from '../components/layout/meta';
import { PageFrame } from '../components/layout/page';
import { Masthead } from '../components/layout/masthead';
import { Content } from '../components/layout/content';
import { Chapters } from '../components/layout/chapters';

import { Outline } from '../components/outline';
import type { NextPage } from 'next';

import { markdownToHTML } from '../lib/markdown';
import { DocumentTree } from '../lib/types';
import { getDocumentBySlug } from '../lib/document';
import { getPackageVersion } from '../lib/gen/fs';

type Props = {
  doc: Document,
  path: string[],
  html: string,
  chapters: [string, number][],
  version: string,
};

const Home: NextPage<Props> = (props: Props) => {
  const {doc, path, html, chapters, version} = props;

  return (<>
    <Meta title={doc.title} />
    <PageFrame
      top={<Masthead version={version} />}
      left={<Outline selected={path} />}
      main={<Content html={html} />}
      right={<Chapters chapters={chapters} />}
    />
  </>)
}

export const getStaticProps = async () => {
  const version = getPackageVersion();

  const doc = getDocumentBySlug("");
  const path = [""];

  if (!doc) throw new Error("index.md not found");

  const [html, chapters] = await markdownToHTML(doc.content);

  return {
    props: { doc, path, html, chapters, version },
  }
}

export default Home;
