---
title: Background
order: 2
---

# Background

<img src="/images/inspect.png" alt="Live inspector" style="width: 800px; max-width: 100%;">

These articles discuss the technical background of **Use.GPU** and the architecture of the **Live** effect run-time.


### Use.GPU

- [The Case for Use.GPU](https://acko.net/blog/the-case-for-use-gpu/)<br>Why GPU programming is so different, why existing approaches fall short, and how to fix it.

- [Frickin' Shaders with Frickin' Laser Beams](https://acko.net/blog/frickin-shaders-with-frickin-laser-beams/)<br>On shader closures and the Use.GPU shader linker and tree shaker.

- [On Variance and Extensibility](https://acko.net/blog/on-variance-and-extensibility/)<br>
  How to design extensible, composable systems.

### Live

These articles explain Effect-based programming and how to combine it with a data-flow and memoization perspective.

 - [Climbing Mount Effect](https://acko.net/blog/climbing-mt-effect/) - Effect-based programming
 - [Reconcile All The Things](https://acko.net/blog/reconcile-all-the-things/) - Memoization and reconciliation
 - [Live - Headless React](https://acko.net/blog/live-headless-react/) - Live run-time and WebGPU
