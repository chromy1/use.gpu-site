import { DocumentIndex, PackageFile, MarkdownFile, TreeNode, IdNode, Document, DocumentNode } from '../types';

import { basename } from 'path';
import { getAllPackages, getAllMarkdown, getAllTSDocs } from './fs';
import { packageTsToDocs } from './tsdocs';
import uniq from 'lodash';

export const buildDocumentTree = () => {
  const markdown = getAllMarkdown();
  const packages = getAllPackages();

  const mdDocs = markdown.map(markdownToDocument);
  const pkgDocs = packages.map(packageToDocument);

  const tsDocs = getAllTSDocs();

  const documents = [
    ...mdDocs,
    ...pkgDocs,
    ...packages.flatMap((pkg, i) => packageTsToDocs(pkg, pkgDocs[i], tsDocs)),
  ];

  const nodes = buildDocIndex(documents);
  const indexed = indexContents(documents);

  const slugs: Record<string, Document> = {};
  for (const doc of documents) slugs[doc.slug] = doc;

  return {documents, slugs, nodes, indexed};
};

export const toNode = <T>(props: T, children?: TreeNode<T>[]): TreeNode<T> => ({
  props,
  children,
});

export const markdownToDocument = (md: MarkdownFile): Document => {
  const {content, meta, file} = md;
  const {title} = meta;

  const slug = file
    .split(/\/docs\//)[1]
    .replace(/(^|\/)index.md$/, '')
    .replace(/\//, '-')
    .replace(/\.md$/, '');

  const base = file
    .split(/\/docs\//)[1]
    .replace(/\/index.md$/, '')
    .replace(/\.md$/, '') + '/';

  return {
    title,
    content,
    href: !meta.empty ? (slug != '' ? `/docs/${slug}` : '/') : undefined,
    icon: 'description',
    slug: slug,
    base,

    namespace: 'docs',
    prefix: undefined,
    keywords: meta.keywords,
    order: meta.order,
    open: !!meta.open || undefined,
    empty: !!meta.empty || undefined,
    group: !!meta.group || undefined,
    score: 5,
  };
};

export const packageToDocument = (pkg: PackageFile): Document => {
  const {content, meta} = pkg;
  const [prefix, title] = meta.name.split('/');
  
  const keywords = meta.keywords ?? [];
  const category = (keywords.find((k: string) => k.match(/^use-gpu-/)) ?? '').split('use-gpu-')[1];

  const slug = "reference-" + (category ? `${category}-` : '') + meta.name.replace(/\//g, '-');
  const icon = `<span style="float: right" className="m-icon m-icon-large" title="Package">inventory_2</span>`;

  const tagged = content ? content.replace(/^ *# /, '# ' + icon + ' ') : '';

  return {
    title,
    content: tagged,
    href: `/docs/${slug}`,
    icon: 'inventory_2',
    slug,
    base: category ? `reference/${category}/${slug}/` : undefined,

    namespace: meta.name.split('/')[1],
    prefix: prefix ? `${prefix}/` : undefined,
    order: meta.order,
    score: 1,
  };
};

export const buildDocIndex = (docs: Document[]): TreeNode<string>[] => {

  docs = docs.slice();
  docs.sort((a, b) => {
    return a.base && b.base ? a.base.localeCompare(b.base) : 0;
  });

  const lookup = new Map<string, Document>();
  for (const doc of docs) {
    const {slug} = doc; 
    if (lookup.has(slug)) console.warn(`Duplicate document ID '${slug}`);
    lookup.set(slug, doc);
  }

  const allNodes: IdNode[] = [];
  const rootNodes: IdNode[] = [];

  nextDoc: for (const doc of docs) {
    const {slug, base} = doc;
    const node = toNode(slug);

    const parents: IdNode[] = [];
    for (const node of allNodes) {
      const slug = node.props;
      const b = lookup.get(slug)!.base;
      if (base != b && base != null && b != null && base.indexOf(b) === 0) {
        parents.push(node);
      }
    }

    allNodes.push(node);

    if (parents.length) {
      const parent = parents.pop()!;
      parent.children = parent.children ?? [];
      parent.children!.push(node);
    }
    else {
      rootNodes.push(node);
    }
  }

  const traverseAll = (nodes: IdNode[], f: (n: IdNode[]) => void) => {
    f(nodes);
    for (const node of nodes) {
      if (!node.children) continue;
      traverseAll(node.children, f);
    }
  }

  traverseAll(rootNodes, (nodes: IdNode[]) => {
    nodes.sort((ak, bk) => {
      const a = lookup.get(ak.props)!;
      const b = lookup.get(bk.props)!;

      const ao = a.order || 0;
      const bo = b.order || 0;
      if (ao - bo) return ao - bo;

      const ast = a.title;
      const bst = b.title;
      return ast.localeCompare(bst);
    });
  })
  
  return rootNodes;
};

const IGNORE_TOKENS = new Set(['any', 'boolean', 'className', 'const', 'enum', 'for', 'float', 'extends', 'function', 'let', 'hook', 'icon', 'interface', 'null', 'number', 'right', 'span', 'src', 'string', 'style', 'tsx', 'type', 'undefined', 'void']);
export const indexContents = (docs: Document[]) => {

  const indexMarkdown = (md: string): string[] => {
    const tokens = markdownToTokens(md).filter(token => (
      (token.length >= 3) && !IGNORE_TOKENS.has(token)
    ));
    return tokens;
  };

  const counts = new Map<string, number>();
  const adopt = (ts: string[]) => {
    for (const s of ts) {
      counts.set(s, (counts.get(s)||0) + 1);
    }
  }

  const out: Document[] = [];
  for (const doc of docs) {
    const terms = indexMarkdown(doc.content);
    const unique = Array.from(new Set(terms));
    const keywords = [...(doc.keywords ?? []), ...unique];
    adopt(terms);
    
    out.push({
      ...doc,
      keywords,
      content: '',
    });
  }

  return out;
};

// would like to use unified/rehype here but ESM blows up ts-node
export const markdownToTokens = (text: string) => {
  const re = /<|```([a-z0-9]+)?(?=\s)|`/g;
  let out = [];

  const tokenize = (s: string) => s
    .split(/(?=[\s,()\[\]<>:\.,/])|(?<=[\s,()\[\]<>:\.,/])/)
    .filter(s => s.match(/[^\s]/))
    .map(t => t.replace(/[^A-Za-z0-9]+/g, '').toLowerCase())
    .filter(t => t.length);

  let pos = 0;

  let m: any;
  while (m = re.exec(text)) {
    const [match] = m;
    const {index} = m;
    
    // Ordinary markdown
    const lines = text.slice(pos, index);
    const headers = lines.split("\n").filter((line: string) => line.match(/^\s*#/)).join("\n");
    out.push(...tokenize(headers));

    const i = index + match.length;
    if (match[0] === '<') {
      // Skip HTML <tag>
      const l = text.indexOf('>', i);
      if (l >= 0) pos = re.lastIndex = l + 1;
      else pos = re.lastIndex = i;
    }
    else if (match.length === 1) {
      // Index code span
      const l = text.indexOf('`', i);
      if (l >= 0) {
        const code = text.slice(i, l);
        out.push(...tokenize(code));
        pos = re.lastIndex = l + 1;
      }
      else pos = re.lastIndex = i;
    }
    else {
      // Skip code block
      const l = text.indexOf('```', i);
      if (l >= 0) pos = re.lastIndex = l + 3;
      else pos = re.lastIndex = i;
    }
  }
  
  const lines = text.slice(pos);
  const headers = lines.split("\n").filter((line: string) => line.match(/^\s*#/)).join("\n");
  out.push(...tokenize(headers));

  return out;
};
